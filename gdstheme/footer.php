<footer class="hide-for-print">
  <?php if(get_field('image_or_color', 'option') == 'color') : ?>
    <div id="parallax3" class="contact-form parallax section" style="background-color: <?php the_field('background_color', 'option'); ?>">
	    <div class="row">
	      <div class="large-12 columns wow fadeIn" data-wow-offset="300">
	        <?php the_field('footer_tier_1_content', 'option'); ?>
	      </div> <!-- /.columns -->
	    </div><!--/.row-->
    </div> <!-- /#parallax3 .contact-form -->
  <?php elseif(get_field('image_or_color', 'option') == 'image') : ?>
  <?php
    $image = get_field('tier_1_parallax_image', 'option');

    // vars
    $url = $image['url'];
    $title = $image['title'];
    $alt = $image['alt'];
    $caption = $image['caption'];

    // image size
    $size = 'full';
    $thumb = $image['sizes'][ $size ];
    $width = $image['sizes'][ $size . '-width' ];
    $height = $image['sizes'][ $size . '-height' ];

    if( !empty($image) ):
  ?>
    <div
      id="parallax3"
      class="contact-form parallax"
      style="background-image: url(<?php echo $image['url']; ?>); background-repeat: no-repeat;"
      data-start="background-position: center 0%"
      data-end="background-position: center 100%"
    >    	
	  <div class="row   
		  <?php
			$yes = get_field('add_form');
			if( $yes && in_array('yes', $yes) ):
			
			// override $post
			$form = $yes;
			setup_postdata( $form );
			?>
				
				no-form
				
			<?php endif;?>" >
				
      <div class="large-12 columns wow fadeIn" data-wow-offset="300">
        <?php the_field('footer_tier_1_content', 'option'); ?>
      </div> <!-- /.columns -->
	  </div>
    </div><!-- /#parallax3 .contact-form -->
  <?php endif;else : endif; ?>
  <div class="footer-tier-2">
    <div class="sitemap wow fadeIn row" data-wow-offset="300">
      <div class="large-12 columns">
        <?php if( get_field( 'footer_navigation', 'option' ) ) : ?>
          <?php
            $main_menu = get_field('footer_navigation', 'option');
            $defaults = array(
              'theme_location'  => 'primary_menu',
              'menu'            => $main_menu,
              'container'       => 'div',
              'container_class' => 'menu menu-centered',
              'container_id'    => '',
              'menu_class'      => 'menu vertical large-horizontal',
              'menu_id'         => 'main_menu',
              'echo'            => true,
              'fallback_cb'     => 'wp_page_menu',
              'before'          => '',
              'after'           => '',
              'link_before'     => '',
              'link_after'      => '',
              'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
              'depth'           => 1,
              'walker' 		  => new GDS_Walker()
            );
            wp_nav_menu( $defaults );
          ?>

      <?php endif; ?>
      </div> <!-- /.columns -->
    </div> <!-- /.sitemap -->   
    
    <div class="credits row">
      <div class="large-12 columns">
        <?php the_field('footer_tier_2_content', 'option'); ?>
      </div> <!-- /.columns -->
    </div> <!-- /.credits -->
	
	<?php if(get_field('social_media_navigation', 'option') ) : ?>
		<div class="social wow fadeIn row" data-wow-offset="200">
	      <div class="large-12 columns">        
	          <?php the_field('social_media_navigation', 'option'); ?>      
	      </div> <!-- /.columns -->
	    </div> <!-- /.social -->  
    <?php endif; ?>  
    
	<div class="footer-logo row">
		<div class="large-3 small-6 small-centered large-centered columns">
			<a href="<?php bloginfo('url'); ?>">
				<?php
					$image = get_field('footer_logo', 'option');
					if( !empty($image) ): ?>
					<img src="<?php echo $image['url']; ?>" class="float-center" alt="<?php echo $image['alt']; ?>" />
				<?php endif; ?>
			</a>
		</div> <!-- /.columns -->
	</div> <!-- /.social --> 
    
  </div><!-- /.footer-tier-2-->

</footer>

</div>
</div> <!-- Close offcanvas wrappers -->
</div>
<?php wp_footer(); ?>
</body>
</html>


<div class="large-12 columns">
  <div class="feed">
    <?php

      $products = new WP_Query(array(
        'post_type' => 'products',
        'post_parent' => 0
      ));

    ?>
    <div class="service-icons">

        <div class="row small-up-1 medium-up-<?php echo $cols; ?>" data-equalizer>
          <?php while($products->have_posts()) { $products->the_post(); ?>
            <div class="column">
              <a href="<?php the_permalink(); ?>">
                <div class="row">
                  <div class="large-12 columns" data-equalizer-watch>
                    <div class="image-container">
                      <?php
                        $image = get_field('product_image');
                        if( !empty($image) ): ?>
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" width="<?php echo $image['width']; ?>" height="<?php echo $image['height']; ?>" />
                      <?php endif; ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="large-12 columns">
                    <h4 class="text-center"><?php the_title(); ?></h4>
                  </div>
                </div>
              </a>
              <div class="row">
                <div class="large-12 columns text-center">
                  <?php the_field('description'); ?>
                </div>
              <div class="row">
                <div class="large-12 columns text-center">
                  <a href="<?php echo the_permalink(); ?>" class="button">Discover <?php the_title();?></a>
                </div>
              </div>
            </div>
          <?php }; ?>
        </div>

      <?php wp_reset_postdata(); ?>
    </div>
  </div>
</div>

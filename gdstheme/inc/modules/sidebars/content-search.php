<div id="sidebar">

	<?php if( get_field('call_to_action', 'option') ): ?>
	    <aside class="widget text-widget">
		  	<?php the_field('call_to_action', 'option'); ?>
	    </aside> <!-- /.text-widget -->
	<?php endif; ?>

	<?php if( get_field('customer_review', 'option') ): ?>
	    <aside class="widget text-widget">
	      <div class="sidebar-testimonial">
		  	<?php the_field('customer_review', 'option'); ?>
	      </div> <!-- /.sidebar-testimonial -->
	    </aside> <!-- /.testimonial-widget -->
	<?php endif; ?>

</div> <!-- /#sidebar -->
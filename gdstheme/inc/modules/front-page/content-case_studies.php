<?php if(is_post_type_archive('case-studies') ) : ?>

	<div class="gallery-items">

		<?php
			$posts = new WP_Query(array(
				'post_type' => 'case-studies',
				'posts_per_page' => 4,
				'orderby' => 'date',
				'order' => 'ASC'
				)
			);
			while ( $posts->have_posts() ) : $posts->the_post();
		?>

		<div class="gallery-item">
			<?php the_post_thumbnail('mini-gallery'); ?>
          	<div class="gallery-item-info">
            	<div class="gallery-item-info-wrap">
              	<div class="caption"><?php the_title(); ?></div> <!-- /.title -->
              	<div class="location"><?php echo $image['caption']; ?></div> <!-- /.location -->
              	<div class="icon icon-expand">
                  	<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
              	</div> <!-- /.icon-expand -->
            	</div> <!-- /.gallery-item-info-wrap -->
          	</div> <!-- /.gallery-item-info -->
		</div><!-- /.gallery-item -->

		<?php endwhile; wp_reset_postdata(); ?>

	</div><!-- /.gallery-items -->

<?php else : endif; ?>

	<?php
		$field = get_sub_field_object( 'case_studies' );
		$post_objects = get_sub_field('case_studies');
		if( $post_objects ):
	?>

      <section class="idea-gallery idea-gallery_case section" data-field="<?php echo $field['key']; ?>">
        <div class="wrap">

			<h1><?php the_sub_field('section_title_case'); ?></h1>

			<div class="gallery-container">

				<div class="gallery-items clearfix">

				    <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
				        <?php setup_postdata($post); ?>

				          <div class="gallery-items">

				          	<div class="gallery-item">

				            	<a href="<?php the_permalink(); ?>">
					              	<div class="gallery-item-img">
										<?php the_post_thumbnail('mini-gallery'); ?>
									</div> <!-- /.gallery-item-img -->
					              	<div class="gallery-item-info">
					                	<div class="gallery-item-info-wrap">
					                  	<div class="caption"><?php the_title(); ?></div> <!-- /.title -->
					                  	<div class="location"><?php the_field('customer_location'); ?></div> <!-- /.location -->
					                  	<div class="hoverlink fa fa-link">&nbsp;</div> <!-- /.icon-expand -->
					                	</div> <!-- /.gallery-item-info-wrap -->
					              	</div> <!-- /.gallery-item-info -->
				            	</a>

				          	</div> <!-- /.gallery-item -->

				          </div> <!-- /.gallery-items -->

				    <?php endforeach; ?>

				</div><!-- /.gallery-items -->

			</div><!-- /.gallery-container -->

		    <a href="<?php echo get_post_type_archive_link( 'case-studies' ); ?>" class="button"><?php the_sub_field('button_content_case'); ?></a>

        </div> <!-- /.wrap -->
      </section> <!-- /.idea-gallery -->

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; // end of $post_objects ?>
<?php
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_5873daa6b85cb',
	'title' => 'Form Options',
	'fields' => array (
		array (
			'layout' => 'vertical',
			'choices' => array (
				'yes' => 'Yes',
			),
			'default_value' => array (
			),
			'allow_custom' => 0,
			'save_custom' => 0,
			'toggle' => 0,
			'return_format' => 'value',
			'key' => 'field_5873dc5ffd6df',
			'label' => 'Would you like to add a form to the sidebar?',
			'name' => 'add_form',
			'type' => 'checkbox',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
		),
		array (
			'default_value' => '',
			'maxlength' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'key' => 'field_5873dc29fd6de',
			'label' => 'Form Title',
			'name' => 'form_title',
			'type' => 'text',
			'instructions' => 'The title shown above your form.',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_5873dc5ffd6df',
						'operator' => '==',
						'value' => 'yes',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
		),
		array (
			'default_value' => '',
			'maxlength' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'key' => 'field_5873dab101a57',
			'label' => 'Form ID',
			'name' => 'form_id',
			'type' => 'text',
			'instructions' => 'Please Insert the form ID Number',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_5873dc5ffd6df',
						'operator' => '==',
						'value' => 'yes',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'page',
			),
			array (
				'param' => 'page_type',
				'operator' => '!=',
				'value' => 'front_page',
			),
		),
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post',
			),
		),
		array (
			array (
				'param' => 'taxonomy',
				'operator' => '==',
				'value' => 'category',
			),
		),
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'products',
			),
		),
	),
	'menu_order' => 1,
	'position' => 'side',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;

 ?>

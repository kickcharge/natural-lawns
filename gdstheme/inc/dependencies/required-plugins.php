<?php

/**
 * This file will load all required plugins
 * This will include ACF Pro, Gravity Forms & Yoast SEO
 */

require_once dirname( __FILE__ ) . '/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'gdstheme_require_plugins' );

function gdstheme_require_plugins() {

    $plugins = array(
	   // Include Advanced Custom Fields Pro
	   array(
	        'name'               => 'Advanced Custom Fields Pro',
	        'slug'               => 'advanced-custom-fields-pro',
	        'source'             => get_template_directory_uri() . '/inc/plugins/advanced-custom-fields-pro.zip', // The "internal" source of the plugin.
	        'required'           => true, // this plugin is required
	        'version'            => '', // the user must use version 1.2 (or higher) of this plugin
	        'force_activation'   => true, // this plugin is going to stay activated unless the user switches to another theme
	        'force_deactivation' => true, // forces the specified plugin to be deactivated when the current theme is switched. This is useful for deactivating theme-specific plugins.
	    ),
	    // Include Gravity Forms
	    array(
	        'name'               => 'Gravity Forms',
	        'slug'               => 'gravityforms',
	        'source'             => get_template_directory_uri() . '/inc/plugins/gravityforms_1.9.18.5.zip', // The "internal" source of the plugin.
	        'required'           => true, // this plugin is required
	        'version'            => '', // the user must use version 1.2 (or higher) of this plugin
	        'force_activation'   => true, // this plugin is going to stay activated unless the user switches to another theme
	        'force_deactivation' => true, // forces the specified plugin to be deactivated when the current theme is switched. This is useful for deactivating theme-specific plugins.
	    ),
      // Include DP Article Social Share
      array(
 	        'name'               => 'DP Article Social Share',
 	        'slug'               => 'dpArticleShare',
 	        'source'             => get_template_directory_uri() . '/inc/plugins/codecanyon-6247263-wordpress-article-social-share.zip', // The "internal" source of the plugin.
 	        'required'           => true, // this plugin is required
 	        'version'            => '', // the user must use version 1.2 (or higher) of this plugin
 	        'force_activation'   => true, // this plugin is going to stay activated unless the user switches to another theme
 	        'force_deactivation' => true, // forces the specified plugin to be deactivated when the current theme is switched. This is useful for deactivating theme-specific plugins.
 	    ),
	    // Include Menu Management Enhancer
	    array(
	        'name'               => 'Menu Management Enhancer',
	        'slug'               => 'menu-management-enhancer-for-wordpress',
	        'source'             => get_template_directory_uri() . '/inc/plugins/menu-management-enhancer-for-wordpress.zip', // The "internal" source of the plugin.
	        'required'           => true, // this plugin is required
	        'version'            => '', // the user must use version 1.2 (or higher) of this plugin
	        'force_activation'   => true, // this plugin is going to stay activated unless the user switches to another theme
	        'force_deactivation' => true, // forces the specified plugin to be deactivated when the current theme is switched. This is useful for deactivating theme-specific plugins.
	    ),
	   // Include WPMU Dev Dashboard (Needed for installing WP Smush Pro and running updates on it)
	   array(
	        'name'               => 'WPMU DEV Dashboard',
	        'slug'               => 'wpmudev-updates',
	        'source'             => get_template_directory_uri() . '/inc/plugins/54475_wpmu-dev-dashboard-4.0.9.zip', // The "internal" source of the plugin.
	        'required'           => true, // this plugin is required
	        'version'            => '', // the user must use version 1.2 (or higher) of this plugin
	        'force_activation'   => true, // this plugin is going to stay activated unless the user switches to another theme
	        'force_deactivation' => true, // forces the specified plugin to be deactivated when the current theme is switched. This is useful for deactivating theme-specific plugins.
	    ),
	   // Include WPMU Dev Dashboard (Needed for installing WP Smush Pro and running updates on it)
	   array(
	        'name'               => 'WP Smush Pro',
	        'slug'               => 'wp-smush-pro',
	        'source'             => get_template_directory_uri() . '/inc/plugins/54475_wp-smush-pro-2.2.2.zip', // The "internal" source of the plugin.
	        'required'           => true, // this plugin is required
	        'version'            => '', // the user must use version 1.2 (or higher) of this plugin
	        'force_activation'   => true, // this plugin is going to stay activated unless the user switches to another theme
	        'force_deactivation' => true, // forces the specified plugin to be deactivated when the current theme is switched. This is useful for deactivating theme-specific plugins.
	    ),
	   // Include WPMU Hummingbird for cahcing (works in conjunction with WP Super Cache)
	   array(
	        'name'               => 'WP Hummingbird',
	        'slug'               => 'wp-hummingbird',
	        'source'             => get_template_directory_uri() . '/inc/plugins/54475_hummingbird-1.2.zip', // The "internal" source of the plugin.
	        'required'           => false, // this plugin is required
	        'version'            => '', // the user must use version 1.2 (or higher) of this plugin
	        'force_activation'   => true, // this plugin is going to stay activated unless the user switches to another theme
	        'force_deactivation' => true, // forces the specified plugin to be deactivated when the current theme is switched. This is useful for deactivating theme-specific plugins.
	    ),
		// Since Yoast SEO is free we will load it direct from the WP Plugin Repo.
		array(
			'name'      => 'Yoast SEO',
			'slug'      => 'wordpress-seo',
			'required'  => true,
		),
		array(
			'name'      => 'Advanced Custom Fields: Nav Menu Field',
			'slug'      => 'advanced-custom-fields-nav-menu-field',
			'required'  => true,
		),
		array(
			'name'      => 'Nested Pages',
			'slug'      => 'wp-nested-pages',
			'required'  => true,
		),
		array(
			'name'      => 'WP Super Cache',
			'slug'      => 'wp-super-cache',
			'required'  => false,
		),
    );

	$config = array(
		array(
		    'id'           => 'gdstheme-tgmpa', // your unique TGMPA ID
		    'default_path' => get_template_directory_uri() . '/inc/plugins/', // default absolute path
		    'menu'         => 'gdstheme-install-required-plugins', // menu slug
		    'has_notices'  => true, // Show admin notices
		    'dismissable'  => false, // the notices are NOT dismissable
		    'dismiss_msg'  => 'I really, really need you to install these plugins, okay?', // this message will be output at top of nag
		    'is_automatic' => true, // automatically activate plugins after installation
		    'message'      => '<!--Hey there.-->', // message to output right before the plugins table
		    //'strings'      => array(); // The array of message strings that TGM Plugin Activation uses
		),
	);

    tgmpa( $plugins, $config );

}

?>

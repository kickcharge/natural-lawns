<?php

if ( ! function_exists('testimonials') ) {

// Register Custom Post Type
function testimonials() {

	$labels = array(
		'name'                => _x( 'Testimonials', 'Post Type General Name', 'gdstheme' ),
		'singular_name'       => _x( 'Testimonial', 'Post Type Singular Name', 'gdstheme' ),
		'menu_name'           => __( 'Testimonials', 'gdstheme' ),
		'name_admin_bar'      => __( 'Testimonials', 'gdstheme' ),
		'parent_item_colon'   => __( 'Parent Item:', 'gdstheme' ),
		'all_items'           => __( 'All Testimonials', 'gdstheme' ),
		'add_new_item'        => __( 'Add New Testimonial', 'gdstheme' ),
		'add_new'             => __( 'Add New', 'gdstheme' ),
		'new_item'            => __( 'New Testimonial', 'gdstheme' ),
		'edit_item'           => __( 'Edit Testimonial', 'gdstheme' ),
		'update_item'         => __( 'Update Testimonial', 'gdstheme' ),
		'view_item'           => __( 'View Testimonial', 'gdstheme' ),
		'search_items'        => __( 'Search Testimonials', 'gdstheme' ),
		'not_found'           => __( 'Not found', 'gdstheme' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'gdstheme' ),
	);
	$rewrite = array(
		'slug'                => 'testimonials',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'label'               => __( 'Testimonial', 'gdstheme' ),
		'description'         => __( 'This section is dedicated to creating testimonials within your website.', 'gdstheme' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', ),
		'taxonomies'          => array(''),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-format-status',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'page',
	);
	register_post_type( 'testimonials', $args );

}
add_action( 'init', 'testimonials', 0 );

}

?>
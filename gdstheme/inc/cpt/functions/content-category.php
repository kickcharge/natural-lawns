	<div class="products-container">

	<?php
		$catargs = array(
			'orderby' => 'name',
			'order' => 'ASC',
		);
		$categories = get_categories( $catargs );
		foreach ($categories as $category) {

		$image = get_field('category_image', $category);
		$size = 'medium';
		$thumb = $image['sizes'][ $size ];
		$width = $image['sizes'][ $size . '-width' ];
		$height = $image['sizes'][ $size . '-height' ];
	?>

		<div id="post-<?php the_ID(); ?>" <?php post_class('col-3 product-category'); ?>>
			
			<?php if($image) { ?>

				<a href="<?php echo get_category_link($category->term_id); ?>">
					<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="295" height="205">
				</a>
				
			<?php } else { ?>
				
				<a href="<?php echo get_category_link($category->term_id); ?>">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/img/product-fallback.jpg" alt="DDI Signs">
				</a>
				
			<?php } ?>
				
			<h3><a href="<?php echo get_category_link($category->term_id); ?>"><?php echo $category->name; // Category title ?></a></h3>
			
			<a class="button yellow" href="<?php echo get_category_link($category->term_id); ?>">View More</a>

		</div><!-- /.product -->

	<?php } ?>

	</div><!-- /.products-container -->
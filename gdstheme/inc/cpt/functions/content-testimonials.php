	<div class="testimonial-container">

		<?php
			$posts = new WP_Query(array(
				'post_type' => 'testimonials',
				'posts_per_page' => -1,
				'orderby' => 'date',
				'order' => 'ASC'
				)
			);
			while ( $posts->have_posts() ) : $posts->the_post();
		?>

	      	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	          	<?php the_content(); ?>

	      	</div><!-- /.testimonials -->

		<?php endwhile; wp_reset_postdata(); ?>

	</div><!-- /.testimonial-container -->
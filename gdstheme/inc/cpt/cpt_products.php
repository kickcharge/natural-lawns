<?php

if ( ! function_exists('products') ) {

// Register Custom Post Type
function products() {

	$labels = array(
		'name'                => _x( 'Products', 'Post Type General Name', 'gdstheme' ),
		'singular_name'       => _x( 'Product', 'Post Type Singular Name', 'gdstheme' ),
		'menu_name'           => __( 'Products', 'gdstheme' ),
		'name_admin_bar'      => __( 'Products', 'gdstheme' ),
		'parent_item_colon'   => __( 'Parent Item:', 'gdstheme' ),
		'all_items'           => __( 'All Products', 'gdstheme' ),
		'add_new_item'        => __( 'Add New Product', 'gdstheme' ),
		'add_new'             => __( 'Add New', 'gdstheme' ),
		'new_item'            => __( 'New Product', 'gdstheme' ),
		'edit_item'           => __( 'Edit Product', 'gdstheme' ),
		'update_item'         => __( 'Update Product', 'gdstheme' ),
		'view_item'           => __( 'View Product', 'gdstheme' ),
		'search_items'        => __( 'Search Products', 'gdstheme' ),
		'not_found'           => __( 'Not found', 'gdstheme' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'gdstheme' ),
	);
	$rewrite = array(
		'slug'                => 'products',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'label'               => __( 'Product', 'gdstheme' ),
		'description'         => __( 'This section is dedicated to creating products within your website.', 'gdstheme' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'page-attributes' ),
		'taxonomies'          => array(''),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-cart',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'page',
	);
	register_post_type( 'products', $args );

}
add_action( 'init', 'products', 0 );

}

?>

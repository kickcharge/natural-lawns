<?php

if ( ! function_exists('ctas') ) {

// Register Custom Post Type
function ctas() {

	$labels = array(
		'name'                => _x( 'CTAs', 'Post Type General Name', 'gdstheme' ),
		'singular_name'       => _x( 'CTA', 'Post Type Singular Name', 'gdstheme' ),
		'menu_name'           => __( 'CTA\'s', 'gdstheme' ),
		'name_admin_bar'      => __( 'CTA\'s', 'gdstheme' ),
		'parent_item_colon'   => __( 'Parent Item:', 'gdstheme' ),
		'all_items'           => __( 'All Calls to Actions', 'gdstheme' ),
		'add_new_item'        => __( 'Add New CTA', 'gdstheme' ),
		'add_new'             => __( 'Add New', 'gdstheme' ),
		'new_item'            => __( 'New CTA', 'gdstheme' ),
		'edit_item'           => __( 'Edit CTA', 'gdstheme' ),
		'update_item'         => __( 'Update CTA', 'gdstheme' ),
		'view_item'           => __( 'View CTA', 'gdstheme' ),
		'search_items'        => __( 'Search CTA\'s', 'gdstheme' ),
		'not_found'           => __( 'Not found', 'gdstheme' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'gdstheme' ),
	);
	$rewrite = array(
		'slug'                => 'cta',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'label'               => __( 'CTA', 'gdstheme' ),
		'description'         => __( 'This section is dedicated to creating custom CTA\'s.', 'gdstheme' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 4,
		'menu_icon'           => 'dashicons-star-empty',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'page',
	);
	register_post_type( 'ctas', $args );

}
add_action( 'init', 'ctas', 0 );

}

?>
<?php get_header(); ?>

<?php get_template_part('inc/modules/content', 'title'); ?>

<div class="content-container">
  <div class="row">
    <?php if (is_category()) : ?>
      <?php get_template_part('inc/modules/loops/loop', 'blog'); ?>
    <?php elseif (is_post_type_archive('products') ) : ?>
      <?php get_template_part('inc/modules/loops/loop', 'products'); ?>
    <?php elseif (is_post_type_archive('coupons') ) : ?>
      <?php get_template_part('inc/modules/loops/loop', 'coupons'); ?>
    <?php else: endif; ?>
  </div>
</div>

<?php get_footer(); ?>

// Child Theme Scripts

jQuery(document).ready(function($) {

	$(document).foundation();
	
	
	// Assign unique ID's to each section on the home page from the builders
	$( 'section[data-field=field_55f734f9496af]',this ).attr( "id", function( arr ) {
		return "open_ended_" + (arr+1);
	});

	$( 'section[data-field=field_55f82d029aed3]',this ).attr( "id", function( arr ) {
		return "open_ended_parallax_" + (arr+1);
	});

	$( 'section[data-field=field_55f7340381455]',this ).attr( "id", function( arr ) {
		return "idea-gallery_" + (arr+1);
	});

	$( 'section[data-field=field_55f73497352c2]',this ).attr( "id", function( arr ) {
		return "idea-gallery_case_" + (arr+1);
	});

	$( 'section[data-field=field_55f732af81454]',this ).attr( "id", function( arr ) {
		return "feed_" + (arr+1);
	});

	$( 'section[data-field=field_55f82fac3a34e]',this ).attr( "id", function( arr ) {
		return "services_" + (arr+1);
	});

	$( 'section[data-field=field_563121aba5fff]',this ).attr( "id", function( arr ) {
		return "testimonial_slider_" + (arr+1);
	});
	
	$( 'section[data-field=field_563cf47caf1d2]',this ).attr( "id", function( arr ) {
		return "custom_content_field_" + (arr+1);
	});
	
	$( 'section[data-field=field_55f82ddd9aed8]',this ).attr( "id", function( arr ) {
		return "statistics_content_field_" + (arr+1);
	});
	
	$( 'section[data-field=field_57275a7e37054]',this ).attr( "id", function( arr ) {
		return "cpt_data_" + (arr+1);
	});

	var main_menu = new Foundation.DropdownMenu($('#main_menu'));
	var main_menu_offcanvas = new Foundation.Drilldown($('#main_menu_offcanvas'), {parentLink: true});

	if ( $('body').hasClass('single-coupons') ) {
		window.print();
	}
	
	$(document).on('click','li.nolink > a',function(e){
		e.preventDefault();
	});
	
	
	//Product Page Functions
	
		// Gallery
		$('#gallery .gallery-item').on('click', function(){
			var img = $(this).find('img').data('img');
			$('#gallery #image-display').html('<img src=' + img + '>');
		});
		
		// Calculator
		$('#calculator select').change(function(){
			var width = $(this).find(":selected").val();
			var length = $('#calculator input').val();
			var price = $('#calculator .total .number').attr('data-price');
			var total = parseFloat(width) * parseFloat(length) * parseFloat(price);
			$('#calculator .total .number').text(total.toFixed(2));
		});
		$('#calculator input').change(function(){
			var length = $(this).val();
			if(length > 25){
				$(this).css('border-color', 'red');
				$('#calculator .total .number').text('0.00');
				return false;
			} else {
				$(this).css('border-color', '#4a7a7f');
			}
			var width = $('#calculator select').find(":selected").val();
			var price = $('#calculator .total .number').attr('data-price');
			var total = parseFloat(width) * parseFloat(length) * parseFloat(price);
			if(isNaN(total)){
				$('#calculator .total .number').text('0.00');
			} else {
				$('#calculator .total .number').text(total.toFixed(2));
			}
		});

// Skrollr
	skrollr.init({
		forceHeight: false,
		mobileCheck: function() {
			return false;
		}
	});

});
		
		
new WOW().init();
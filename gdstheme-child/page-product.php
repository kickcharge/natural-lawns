<?php /* Template Name: Product Page */ ?>
<?php get_header(); ?>

      <?php get_template_part('inc/modules/content', 'title'); ?>
      <div class="content-container">
        <?php if(get_field('breadcrumbs_positioning', 'option') == 'content' && function_exists('yoast_breadcrumb') ) { ?>
        <div class="row breadcrumb-row">
          <div class="medium-12 columns">
            <?php yoast_breadcrumb('<div class="breadcrumbs">','</div>'); ?>
          </div>
        </div>
        <?php } ?>
  			<div class="row">
  	      <div class="medium-8 columns">

  					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  				  		<?php the_content(); ?>
  				  		<?php
	  				  		$gallery = get_post_meta(get_the_ID(), 'gallery', true);
	  				  		$calculator = get_post_meta(get_the_ID(), 'calculator', true);
	  				  		$price = get_post_meta(get_the_ID(), 'price', true);
	  				  		$pileHeight = get_post_meta(get_the_ID(), 'pile_height', true);
	  				  		$widths = get_post_meta(get_the_ID(), 'widths_available', true);
	  				  		$yarn = get_post_meta(get_the_ID(), 'yarn', true);
	  				  		$colours = get_post_meta(get_the_ID(), 'colours', true);
	  				  		$latexBacking = get_post_meta(get_the_ID(), 'latex_backing', true);
	  				  		$grassWeight = get_post_meta(get_the_ID(), 'grass_weight', true);
	  				  		$guarantee = get_post_meta(get_the_ID(), 'product_guarantee', true);
	  				  		$manufactured = get_post_meta(get_the_ID(), 'manufactured', true);
	  				  	?>
  				  		<ul id="product-tabs" class="tabs" data-deep-link="true" data-update-history="true" data-deep-link-smudge="true" data-deep-link-smudge="500" data-tabs>
							<li class="tabs-title is-active"><a href="#gallery" aria-selected="true">Gallery</a></li>
							<li class="tabs-title"><a href="#price-calculator">Price Calculator</a></li>
							<li class="tabs-title"><a href="#specification">Specification</a></li>
						</ul>
						
						<div id="product-tab-content" class="tabs-content" data-tabs-content="product-tabs">
							<div class="tabs-panel is-active" id="gallery">
								<?php if($gallery): ?>
									<?php for($i = 0;$i < count($gallery); $i++){ ?>
										<?php if($i === 0): ?>
											<div id="image-display" class="row column small-12">
												<?php echo wp_get_attachment_image($gallery[$i], 'full'); ?> 
											</div>
											<ul id="gallery-list" class="row large-up-3 small-up-2">
												<li class="gallery-item column column-block">
										<?php else :?>
											<li class="gallery-item column column-block">
										<?php endif; ?>
												<?php 
													$fullSize = wp_get_attachment_image_src($gallery[$i], 'full'); 
													$iconSize = wp_get_attachment_image_src($gallery[$i], 'medium');
													echo wp_get_attachment_image($gallery[$i], 'medium', false, array('data-img' => $fullSize[0]));?>
											</li>
										<?php if(($i+1) == $gallery){ ?>
											</ul>
										<?php } ?>
									<?php } ?>

								<?php else : ?>
									<p>This product does not have any images.</p>
								<?php endif; ?>
							</div>
							<div class="tabs-panel" id="price-calculator">
								<div id='calculator'>
									<div class="roll-width">
										<span class="calc-label"><span>Roll Width </span>(m)</span>
										<select>
											<option value='' disabled selected>Please select</option>
											<?php 
												$options = explode(", ", $widths);
												for($i = 0; $i < count($options); $i++){
													$num = explode('m', $options[$i]);
													echo '<option value='.$num[0].'>'.$options[$i].'</option>';
												}
											?>
										</select>
									</div>
									<div class="icon">
										<span>X</span>
									</div>
									<div class="length">
										<span class="calc-label"><span>Length </span>(max 25m)</span>
										<input type='number' step="0.1" placeholder='0' max='25' value="0">
									</div>
									<div class="icon">
										<span>=</span>
									</div>
									<div class="total">
										<span>£<span class="number" data-price="<?php preg_match_all('!\d+\.*\d*!', $price, $match); echo $match[0][0]; ?>">0.00</span></span>
									</div>
								</div>
							</div>
							<div class="tabs-panel" id="specification">
								<table>
									<?php if($price != ''){ ?>
										<tr>
											<td>Price</td>
											<td><?php echo $price;?></td>
										</tr>
									<?php } ?>
									<?php if($pileHeight != ''){ ?>
										<tr>
											<td>Pile Height</td>
											<td><?php echo $pileHeight;?></td>
										</tr>
									<?php } ?>
									<?php if($widths != ''){ ?>
										<tr>
											<td>Widths Available</td>
											<td><?php echo $widths;?></td>
										</tr>
									<?php } ?>
									<?php if($yarn != ''){ ?>
										<tr>
											<td>Yarn</td>
											<td><?php echo $yarn;?></td>
										</tr>
									<?php } ?>
									<?php if($colours != ''){ ?>
										<tr>
											<td>Colours</td>
											<td><?php echo $colours;?></td>
										</tr>
									<?php } ?>
									<?php if($latexBacking != ''){ ?>
										<tr>
											<td>Latex Backing</td>
											<td><?php echo $latexBacking;?></td>
										</tr>
									<?php } ?>
									<?php if($grassWeight != ''){ ?>
										<tr>
											<td>Grass Weight</td>
											<td><?php echo $grassWeight;?></td>
										</tr>
									<?php } ?>
									<?php if($guarantee != ''){ ?>
										<tr>
											<td>Product Guarantee</td>
											<td><?php echo $guarantee;?></td>
										</tr>
									<?php } ?>
									<?php if($manufactured != ''){ ?>
										<tr>
											<td>Manufactured</td>
											<td><?php echo $manufactured;?></td>
										</tr>
									<?php } ?>
								</table>
							</div>
						</div>
						  				  		
  		        <?php endwhile; ?>
  		      <?php else : ?>
  				  	<h2>Sorry Nothing Found</h2>
  		      <?php endif; ?>
  	      </div>
          <?php get_sidebar('right'); ?>
          <?php get_template_part('inc/acf/page', 'builder'); ?>
        </div>
      </div>

<?php get_footer(); ?>
